/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    fontSize: {
      sm: "0.7rem",
    },
    paddingBottom: {
      "250%": "250%",
      "120%": "120%",
    },
  },
  plugins: [],
};
