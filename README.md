# Nombre del Proyecto 📋

"**AgroTable:** es una aplicación web que funciona como un directorio de productores agrícolas. Permite registrar información relevante de cada productor como ubicación cultivos, extensión de tierra y volumen de producción. Los usuarios pueden consultar el directorio de productores, filtrar por diferentes criterios y ver información de contacto. De esta manera, AgroTable facilita conectar a compradores y vendedores del sector agrícola al proveer un directorio accesible de productores agropecuarios. La aplicación está desarrollada con React y consume una API REST para proveer la información de los productores registrados. 😀

<div style="text-align: center; padding: 10px; display:flex flex-direction:column">
    <h1 style="font-size:25px; text-decoration-line: underline;">Version Escritorio 💻</h1>
    <div style="display:flex; flex-wrap: wrap; gap:5px; justify-content: center;">
    <img src="/public/Desktop1.png" width="300px">
    <img src="/public/Desktop2.png" width="300px">
    <img src="/public/Desktop3.png" width="300px">
    </div>
    <h1 style="font-size:25px; text-decoration-line: underline;">Version Mobile 📱</h1>
    <div style="display:flex; flex-wrap: wrap; gap:5px; justify-content: center;">
    <img src="/public/Mobile1.png" width="200px" height="200px">
    <img src="/public/Mobile2.png" width="200px"height="500px">
    <img src="/public/Mobile3.png" width="200px"height="300px">
    </div>
    
</div>

# Link Proyecto

<div style="display: flex; flex-direction: column; align-items: center;">
    <img src="./public/agro.png" width="50px">
    <a style="color: blue; font-size: 20px; display: block; text-align: center;" href="https://gitlab.com/Willydmq/agrotable" target="_blank">Repo AgroTable"</a>
</div>

## Instalación ⚙️

1. Asegúrese de tener instalado Node.js en su computadora. Si no lo tiene instalado, puede descargar e instalar **Node.js** en el sitio web oficial de Node.js.

2. Descargar o clonar el repositorio del proyecto desde GitHub.

3. Abrir una terminal o línea de comandos en la carpeta raíz del proyecto.

4. Ejecutar el comando npm install para instalar todas las dependencias necesarias del proyecto, incluyendo:

   - ![NPM](https://img.shields.io/badge/@tanstack/matchSorterUtils-NPM-red) <a href="https://www.npmjs.com/package/@tanstack/match-sorter-utils">@tanstack/match-sorter-utils</a>
   - ![NPM](https://img.shields.io/badge/@tanstack/reactTable-NPM-blue) <a href="https://www.npmjs.com/package/@tanstack/react-table">@tanstack/react-table</a>
   - ![NPM](https://img.shields.io/badge/axios-NPM-green) <a href="https://www.npmjs.com/package/axios">axios</a>
   - ![NPM](https://img.shields.io/badge/classnames-NPM-orange) <a href="https://www.npmjs.com/package/classnames">classnames</a>
   - ![NPM](https://img.shields.io/badge/react-NPM-white) <a href="https://www.npmjs.com/package/react">react</a>
   - ![NPM](https://img.shields.io/badge/reactDom-NPM-violet) <a href="https://www.npmjs.com/package/react-dom">react-dom</a>
   - ![NPM](https://img.shields.io/badge/reactRouterDom-NPM-limon) <a href="https://www.npmjs.com/package/react-router-dom">react-router-dom</a>
   - ![NPM](https://img.shields.io/badge/xlsx-NPM-gold) <a href="https://www.npmjs.com/package/xlsx">xlsx</a>
   - ![NPM](https://img.shields.io/badge/postcss-NPM-brown) <a href="https://www.npmjs.com/package/postcss">postcss</a>
   - ![NPM](https://img.shields.io/badge/tailwindcss-NPM-yellow) <a href="https://www.npmjs.com/package/tailwindcss">tailwindcss</a>

   ```
   npm install
   ```

5. Una vez completados los pasos anteriores, ejecutar el comando npm run dev para iniciar la aplicación.
   ```
   npm run dev
   ```
6. Acceder a la URL http://localhost:5173 en un navegador web para ver la aplicación en funcionamiento.

## Construido con 🛠️

<div style="text-align: center; padding: 10px;">
    <img src="/public/vite.png" width="100px">
</div>

## Autores ✒️

- **William Maldonado** - _GALAPADO - ReactJS Challenge_ - [Willydmq](https://gitlab.com/Willydmq)

---

⌨️ con ❤️ por [William Maldonado](https://gitlab.com/Willydmq) 😊😊

---
