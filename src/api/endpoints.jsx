import axios from "axios";

export const fetchClientes = async () => {
  const response = await axios.get(
    "https://639c781616d1763ab14ae929.mockapi.io/test/front-end/lista"
  );

  const productos = [];

  Object.keys(response.data).map((key) => {
    const producto = response.data[key];

    productos.push({
      nombre: producto.nombre,
      apellido: producto.apellido,
      celular: producto.celular,
      documento: producto.documento,
      emailCreacion: producto.emailCreacion,
      estado: producto.estado,
      id: key,
    });
  });
  return productos;
};
