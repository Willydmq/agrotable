import PropTypes from "prop-types";
import { Down, Sort, Up } from "../Icons/Icons";

export const SortIcon = ({
  column,
  sortColumn,
  sortDirection,
  onSort,
  name,
}) => {
  const isSorted = sortColumn === column;

  return (
    <th className="flex items-center justify-center flex-row">
      {name}
      <span className={`ml-2 cursor-pointer`} onClick={() => onSort(column)}>
        {isSorted ? sortDirection === "asc" ? <Up /> : <Down /> : <Sort />}
      </span>
    </th>
  );
};

SortIcon.propTypes = {
  column: PropTypes.string.isRequired,
  sortColumn: PropTypes.string,
  sortDirection: PropTypes.oneOf(["asc", "desc"]),
  onSort: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
};
