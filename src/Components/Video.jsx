import PropTypes from "prop-types";

export const Video = ({ src, alt }) => {
  return (
    <div id="video" className="hidden sm:hidden lg:block">
      <video
        src={src}
        type="video/webm"
        autoPlay
        muted
        loop
        alt={alt}
        className="absolute w-[99%] h-auto"
      ></video>
    </div>
  );
};

Video.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string,
};
