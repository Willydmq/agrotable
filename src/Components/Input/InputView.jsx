import PropTypes from "prop-types";

export const InputView = ({ type, id, label, value }) => {
  return (
    <div>
      <label
        htmlFor={id}
        className="block mb-2 text-sm font-medium text-gray-700"
      >
        {label}
      </label>
      <input
        type={type}
        name={id}
        id={id}
        value={value}
        className="border rounded-lg focus:outline-none focus:border-stone-400 focus:ring-2 focus:ring-stone-400 block w-full p-0 bg-stone-200 border-gray-400 placeholder-gray-400"
      />
    </div>
  );
};

InputView.propTypes = {
  type: PropTypes.string,
  label: PropTypes.string,
  id: PropTypes.string,
  value: PropTypes.string,
};
