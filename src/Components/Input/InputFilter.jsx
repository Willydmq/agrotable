import PropTypes from "prop-types";
import { useEffect, useState } from "react";

export const InputFilter = ({
  value: initValue,
  onChange,
  debounce = 500,
  ...props
}) => {
  const [value, setValue] = useState(initValue);
  useEffect(() => {
    setValue(initValue);
  }, [initValue]);

  // *  0.5s after set value in state
  useEffect(() => {
    const timeout = setTimeout(() => {
      onChange(value);
    }, debounce);
    return () => clearTimeout(timeout);
  }, [value]);

  return (
    <input
      {...props}
      value={value}
      onChange={(e) => setValue(e.target.value)}
    />
  );
};

InputFilter.propTypes = {
  value: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  debounce: PropTypes.number,
};
