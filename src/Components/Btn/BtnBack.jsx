import PropTypes from "prop-types";

export const BtnBack = ({ onClick, name, icon }) => {
  return (
    <button className="download-btn" onClick={onClick}>
      {icon}
      {name}
    </button>
  );
};

BtnBack.propTypes = {
  onClick: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
  icon: PropTypes.element,
};
