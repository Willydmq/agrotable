import { useLocation, useNavigate } from "react-router-dom";
import { BtnBack } from "../Components/Btn/BtnBack";
import { InputView } from "../Components/Input/InputView";
import { Video } from "../Components/Video";
import { Back } from "../Icons/Icons";

export default function FormView() {
  const navigate = useNavigate();

  const location = useLocation();
  const data = location.state;

  function handleSearch() {
    navigate("/search");
  }

  return (
    <section className="bg-gray-100 relative">
      <Video
        src="https://res.cloudinary.com/wdapico/video/upload/v1698380669/c8ux6ufzg2jgcyvsq86h.webm"
        alt="Galapp"
      />
      <div className="flex flex-col items-center justify-center px-6 py-4 mx-auto">
        <div className="flex items-center justify-between mb-2 gap-12 z-10 sm:z-10 lg:z-10">
          <h3 className="text-2xl font-semibold text-gray-800 pl-12">
            Clientes
          </h3>
          <BtnBack onClick={handleSearch} name="Atras" icon={<Back />} />
        </div>
        <div className="w-full bg-[#ffb81e] rounded-lg shadow md:mt-0 sm:max-w-xs xl:p-0 border-[#ffb81e] z-10 sm:z-10 lg:z-10">
          <div className="p-6 space-y-4">
            <form action="" className="space-y-4">
              <InputView
                label="Nombre"
                type="text"
                id="nombre"
                readOnly
                value={data.nombre}
              />
              <InputView
                label="Apellido"
                type="text"
                id="apellido"
                value={data.apellido}
              />
              <InputView
                label="Celular"
                type="text"
                id="celular"
                value={data.celular}
              />
              <InputView
                label="Documento"
                type="text"
                id="dni"
                value={data.documento}
              />
              <InputView
                label="Email"
                type="email"
                id="email"
                value={data.emailCreacion}
              />
              <InputView
                label="Estado"
                type="text"
                id="estado"
                value={data.estado}
              />
            </form>
          </div>
        </div>
      </div>
    </section>
  );
}
