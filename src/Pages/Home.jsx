import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Video } from "../Components/Video";

export default function Home() {
  const navigate = useNavigate();

  function handleSearch() {
    setLoading(true);

    setTimeout(() => {
      setLoading(false);

      navigate("/search");
    }, 5000);
  }

  const [loading, setLoading] = useState(false);

  return (
    <main className="grid place-items-center h-screen relative">
      <Video
        src="https://res.cloudinary.com/wdapico/video/upload/v1698385137/i0at2ijw0ikj8edikn9i.webm"
        alt="GalaAppHome"
      />
      <div className="pb-[250%] sm:pb-[120%] lg:pb-0 z-10">
        <button onClick={handleSearch} className="into">
          Search
        </button>
        {loading && (
          <div className="absolute inset-x-15 bottom-14 pb-[250%] sm:pb-[120%] lg:pb-0">
            Cargando...
          </div>
        )}
      </div>
    </main>
  );
}
