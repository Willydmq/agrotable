import {
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  getPaginationRowModel,
  useReactTable,
} from "@tanstack/react-table";
import classNames from "classnames";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { BtnBack } from "../Components/Btn/BtnBack";
import { BtnDowloand } from "../Components/Btn/BtnDowloand";
import { InputFilter } from "../Components/Input/InputFilter";
import { SortIcon } from "../Components/SortIcon";
import { Back, SearchIcon, View } from "../Icons/Icons";
import { fetchClientes } from "../api/endpoints";

export default function TableProducts() {
  /**--------------- Llamado a la API --------------- */
  const [productos, setProductos] = useState([]);

  useEffect(() => {
    const getProductos = async () => {
      const productos = await fetchClientes();
      const productosWithSortNumber = productos.map((row, i) => ({
        ...row,
        sortNumber: i + 1,
      }));

      setProductos(productosWithSortNumber);
    };

    getProductos();
  }, []);

  /**--------------- Función para manejar ordenamiento --------------- */

  const [sortColumn, setSortColumn] = useState();
  const [sortDirection, setSortDirection] = useState();

  function handleSort(columnId) {
    setSortColumn(columnId);
    setSortDirection((prev) => (prev === "asc" ? "desc" : "asc"));

    const sortedData = [...productos].sort((a, b) => {
      if (a[columnId] < b[columnId]) return sortDirection === "asc" ? -1 : 1;
      if (a[columnId] > b[columnId]) return sortDirection === "asc" ? 1 : -1;
      return 0;
    });

    setProductos(sortedData);
  }

  /**--------------- Creacion Columnas y Filas --------------- */

  const columnHelper = createColumnHelper();

  const columns = [
    columnHelper.accessor("sortNumber", {
      header: () => (
        <SortIcon
          name={"S.No"}
          column="sortNumber"
          sortColumn={sortColumn}
          sortDirection={sortDirection}
          onSort={handleSort}
        />
      ),
      id: "S.No",
      cell: (info) => <span>{info.getValue()}</span>,
    }),
    columnHelper.accessor("nombre", {
      header: () => (
        <SortIcon
          name={"Nombre"}
          column="nombre"
          sortColumn={sortColumn}
          sortDirection={sortDirection}
          onSort={handleSort}
        />
      ),
      cell: (info) => <span>{info.getValue()}</span>,
    }),
    columnHelper.accessor("apellido", {
      header: () => (
        <SortIcon
          name={"Apellido"}
          column="apellido"
          sortColumn={sortColumn}
          sortDirection={sortDirection}
          onSort={handleSort}
        />
      ),
      cell: (info) => <span>{info.getValue()}</span>,
    }),
    columnHelper.accessor("celular", {
      header: () => (
        <SortIcon
          name={"Celular"}
          column="celular"
          sortColumn={sortColumn}
          sortDirection={sortDirection}
          onSort={handleSort}
        />
      ),
      cell: (info) => <span>{info.getValue()}</span>,
    }),
    columnHelper.accessor("documento", {
      header: () => (
        <SortIcon
          name={"Documento"}
          column="documento"
          sortColumn={sortColumn}
          sortDirection={sortDirection}
          onSort={handleSort}
        />
      ),
      cell: (info) => <span>{info.getValue()}</span>,
    }),
    columnHelper.accessor("emailCreacion", {
      header: () => (
        <SortIcon
          name={"Email"}
          column="emailCreacion"
          sortColumn={sortColumn}
          sortDirection={sortDirection}
          onSort={handleSort}
        />
      ),
      cell: (info) => <span>{info.getValue()}</span>,
    }),
    columnHelper.accessor("estado", {
      header: () => (
        <SortIcon
          name={"Estado"}
          column="estado"
          sortColumn={sortColumn}
          sortDirection={sortDirection}
          onSort={handleSort}
        />
      ),
      cell: (info) => (
        <span
          className={classNames({
            "text-white px-2 rounded-full font-semibold": true,
            "bg-red-500": "Inactivo" === info.getValue(),
            "bg-green-500": "Activo" === info.getValue(),
          })}
        >
          {info.getValue()}
        </span>
      ),
    }),
    columnHelper.accessor("view", {
      cell: (info) => (
        <button
          className="rounded-full w-10 h-10 object-cover cursor-pointer"
          onClick={() =>
            navigate(`/view/${info.row.original.id}`, {
              state: info.row.original,
            })
          }
        >
          <View />
        </button>
      ),
      header: "Busqueda",
    }),
  ];

  /**--------------- Manejo de filtros --------------- */

  const [globalFilter, setGlobalFilter] = useState("");

  const table = useReactTable({
    data: productos,
    columns,
    state: {
      globalFilter,
    },
    getFilteredRowModel: getFilteredRowModel(),
    getCoreRowModel: getCoreRowModel(),
    getPaginationRowModel: getPaginationRowModel(),
  });

  /**--------------- Nombre de las Columnas --------------- */

  const columnNames = {
    sortNumber: "S.No",
    nombre: "Nombre",
    apellido: "Apellido",
    celular: "Celular",
    documento: "Documento",
    emailCreacion: "Email",
    estado: "Estado",
    view: "Busqueda",
  };

  /**--------------- navigate --------------- */

  const navigate = useNavigate();

  function handleSearch() {
    navigate("/");
  }

  return (
    <section className="min-h-screen pt-4 bg-white text-sm">
      <div className="max-w-5xl p-2 mx-auto text-black fill-gray-400">
        <div className="flex flex-wrap justify-between mb-2 sm:flex-nowrap">
          <div className="w-full flex items-center gap-1">
            <SearchIcon />
            <InputFilter
              value={globalFilter ?? ""}
              onChange={(value) => setGlobalFilter(String(value))}
              className="p-2 bg-transparent outline-none border-b-2 w-2/6 focus:w-1/3 duration-300 border-[#ffb81e] sm:w-2/6 lg:w-1/5"
              placeholder="Buscando en todas las columnas..."
            />
          </div>
          <div className="flex justify-end w-full gap-2">
            <BtnBack onClick={handleSearch} name="Atras" icon={<Back />} />
            <BtnDowloand data={productos} fileName={"listado"} />
          </div>
        </div>
        <div className="bolck bg-transparent w-full overflow-x-auto md:overflow-x-auto">
          <table className="border border-gray-700 w-full text-left">
            <thead className="bg-[#ffb81e]">
              {table.getHeaderGroups().map((headerGroup) => (
                <tr key={headerGroup.id}>
                  {headerGroup.headers.map((header) => (
                    <th
                      key={header.id}
                      className="capitalize px-3.5 py-2 hidden sm:table-cell"
                      id={columnNames[header.column.id]}
                    >
                      {flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}
                    </th>
                  ))}
                </tr>
              ))}
            </thead>
            <tbody>
              {table.getRowModel().rows.length
                ? table.getRowModel().rows.map((row, i) => (
                    <tr
                      key={row.id}
                      className={`
                ${i % 2 === 0 ? "bg-stone-300" : "bg-stone-200"}
                `}
                    >
                      {row.getVisibleCells().map((cell) => {
                        return (
                          <td
                            key={cell.id}
                            data-cell={columnNames[cell.column.id]}
                            id={columnNames[cell.column.id]}
                            className="px-3.5 py-2 grid grid-cols-2 items-center first:hidden nth last:pb-8 before:content-[attr(data-cell)] before:font-bold before:capitalize sm:table-cell sm:before:content-[''] sm:last:pb-0 sm:first:table-cell"
                          >
                            {flexRender(
                              cell.column.columnDef.cell,
                              cell.getContext()
                            )}
                          </td>
                        );
                      })}
                    </tr>
                  ))
                : null}
            </tbody>
          </table>
        </div>
        {/* Paginacion */}
        <div className="flex flex-col gap-3 items-center sm:flex-row">
          <div className="flex items-center justify-end mt-2 gap-2 w-full">
            <button
              onClick={() => {
                table.previousPage();
              }}
              disabled={!table.getCanPreviousPage()}
              className="p-1 border border-gray-300 px-2 disabled:opacity-30"
            >
              {"<"}
            </button>
            <button
              onClick={() => {
                table.nextPage();
              }}
              disabled={!table.getCanNextPage()}
              className="p-1 border border-gray-300 px-2 disabled:opacity-30"
            >
              {">"}
            </button>
            <span className="flex items-center gap-1">
              <div>Page</div>
              <strong>
                {table.getState().pagination.pageIndex + 1} of{" "}
                {table.getPageCount()}
              </strong>
            </span>
          </div>
          <div className="flex flex-row w-full gap-10 sm:gap-0  lg:w-6/12 sm:w-3/5 mt-2">
            <span className="flex gap-1 w-6/12 items-center sm:gap-0 lg:w-7/12 sm:w-4/6">
              | Ve a la pagina:
              <input
                type="number"
                defaultValue={table.getState().pagination.pageIndex + 1}
                onChange={(e) => {
                  const page = e.target.value ? Number(e.target.value) - 1 : 0;
                  table.setPageIndex(page);
                }}
                className="border p-1 rounded w-16 bg-transparent"
              />
            </span>
            <select
              value={table.getState().pagination.pageSize}
              onChange={(e) => {
                table.setPageSize(Number(e.target.value));
              }}
              className="p-2 bg-transparent w-4/12"
            >
              {[10, 20, 30, 50].map((pageSize) => (
                <option value={pageSize} key={pageSize}>
                  Muestra {pageSize}
                </option>
              ))}
            </select>
          </div>
        </div>
      </div>
    </section>
  );
}
