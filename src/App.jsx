import { Route, Routes } from "react-router-dom";
import FormView from "./Pages/FormView";
import Home from "./Pages/Home";
import TableProducts from "./Pages/TableProducts";

function App() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/search" element={<TableProducts />} />
      <Route path="/view/:id" element={<FormView />} />
    </Routes>
  );
}

export default App;
